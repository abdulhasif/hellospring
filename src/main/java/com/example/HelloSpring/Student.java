package com.example.HelloSpring;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    private Long stdId;

    private String stdName;

    private int age;

    private String dept;

    private String admissionDt;

}