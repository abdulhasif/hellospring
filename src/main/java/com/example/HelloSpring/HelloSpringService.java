package com.example.HelloSpring;

import org.springframework.stereotype.Service;

@Service
public class HelloSpringService {

    public Student getStudent()
    {
        Student std = new Student(1001L,"Hasif",22,"cse","08-Aug-2020");
        return std;
    }
}
