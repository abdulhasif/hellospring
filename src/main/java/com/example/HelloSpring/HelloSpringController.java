package com.example.HelloSpring;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class HelloSpringController {

    private final HelloSpringService helloSpringService;
    private final ModelMapper modelMapper;

    @Autowired
    public HelloSpringController(final HelloSpringService helloSpringService,final ModelMapper modelMapper){
        this.helloSpringService = helloSpringService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/test")
    public String test(){
        return "RUNNING";
    }

    @GetMapping("/getStudent")
    public StudentDTO retrieveStudent(){

        Student std = helloSpringService.getStudent();
        return std == null ? null : this.modelMapper.map(std,StudentDTO.class);
    }

}
